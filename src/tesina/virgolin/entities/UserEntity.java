package tesina.virgolin.entities;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;


@Entity
public class UserEntity {

	@Id private String id;
	
	public UserEntity(){};
	
	public UserEntity(String id){
		this.id = id;
	}
	
	public static Key<UserEntity> getKey(String id){
		return Key.create(UserEntity.class, id);
	}
	
	public String getId() {
		return id;
	}
	

}
