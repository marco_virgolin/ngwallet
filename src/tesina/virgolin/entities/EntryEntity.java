package tesina.virgolin.entities;

import java.util.Date;

import javax.persistence.GeneratedValue;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Parent;

@Entity
public class EntryEntity {
	@Id @GeneratedValue private Long id;
	private Date date;
	private String chapter;
	private float amount;
	private String description;
	@Parent private Key<UserEntity> userKey;
	
	public EntryEntity(){	}
	
	public EntryEntity(Date date, String chapter, float amount, String description, UserEntity user){
		this.setDate(date);
		this.setChapter(chapter);
		this.setAmount(amount);
		this.setDescription(description);
		this.setUser(user);
	}
	
	public EntryEntity(Long id, Date date, String chapter, float amount, String description, UserEntity user){
		this.setId(id);
		this.setDate(date);
		this.setChapter(chapter);
		this.setAmount(amount);
		this.setDescription(description);
		this.setUser(user);
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id){
		this.id = id;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getChapter() {
		return chapter;
	}
	
	public void setChapter(String chapter) {
		this.chapter = chapter;
	}
	
	public float getAmount() {
		return amount;
	}
	
	public void setAmount(float amount) {
		this.amount = amount;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public void setUser(UserEntity user) {
		this.userKey = Key.create(UserEntity.class, user.getId());
	}
	
	public static Key<EntryEntity> getKey(Long id){
		return Key.create(EntryEntity.class, id);
	}
}
