package tesina.virgolin.data;

import static tesina.virgolin.data.OfyService.ofy;

import java.util.List;

import com.google.appengine.api.users.User;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.VoidWork;
import com.googlecode.objectify.Work;

import tesina.virgolin.entities.EntryEntity;
import tesina.virgolin.entities.UserEntity;

public class DataService implements IDataService{
	
	private UserEntity getUserEntityFromGoogleUser(User googleUser){
		/* 
		 * The following commented code tries to load the UserEntity from the datastore.
		 * If the entity is not present in the datastore, then it saves it for the first time
		 * before returning it. 
		 * This was done to achieve the creation of an entity group of EntryEntities that
		 * are children of the UserEntity.
		 *
		 * 	UserEntity ue = ofy().load().type(UserEntity.class).id(googleUser.getEmail()).now();
		 * 	if (ue == null){
		 * 		ue = new UserEntity(googleUser.getEmail());
		 * 		ofy().save().entity(ue).now();
		 * 	}
		 * 	return ue;
		 *
		 *
		 * However, by reading Objectify's documentation @ https://code.google.com/p/objectify-appengine/wiki/Concepts,
		 * you learn that:
		 * "you can create child entites with a parent Key (or any other key as a member field) that points to a nonexistant entity".
		 * Thus, saving the UserEntity in the datastore is not necessary.
		 */
		return new UserEntity(googleUser.getEmail());
	}
	
	@Override
	public List<EntryEntity> getEntries(User googleUser){
		UserEntity ue = getUserEntityFromGoogleUser(googleUser);
		/* Why not using an explicit ofy transaction:
		 * https://groups.google.com/forum/#!msg/objectify-appengine/q61rUzwrufI/lce0BGAgq9YJ
		 */
		return ofy().load().type(EntryEntity.class).ancestor(UserEntity.getKey(ue.getId())).list();
	}
	
	@Override
	public EntryEntity getEntry(Key<EntryEntity> key){
		final Key<EntryEntity> k = key;
		return ofy().load().key(k).now();
	}
	
	@Override
	public Key<EntryEntity> saveEntry(EntryEntity entry){
		final EntryEntity e = entry;
		return ofy().transact(new Work<Key<EntryEntity>>(){
			@Override
			public Key<EntryEntity> run() {
				return ofy().save().entity(e).now();
			}			
		});
	}

	@Override
	public void deleteEntry(Long id, User googleUser) {
		final long i = id;
		final UserEntity ue = getUserEntityFromGoogleUser(googleUser);
		ofy().transact(new VoidWork(){
			public void vrun(){
				ofy().delete().type(EntryEntity.class).parent(UserEntity.getKey(ue.getId())).id(i).now();
			}
		});
	}
}
