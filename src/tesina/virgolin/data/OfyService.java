package tesina.virgolin.data;

import tesina.virgolin.entities.*;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;


public class OfyService {
	static {
		factory().register(UserEntity.class);
		factory().register(EntryEntity.class);
	}
	
	public static Objectify ofy() {
		return ObjectifyService.ofy();
	}
	
	public static ObjectifyFactory factory() {
		return ObjectifyService.factory();
	}
}
