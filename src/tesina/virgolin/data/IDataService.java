package tesina.virgolin.data;

import java.util.List;

import com.google.appengine.api.users.User;
import com.googlecode.objectify.Key;

import tesina.virgolin.entities.EntryEntity;

public interface IDataService {
	public List<EntryEntity> getEntries(User googleUser);
	public EntryEntity getEntry(Key<EntryEntity> key);
	public Key<EntryEntity> saveEntry(EntryEntity entry);
	public void deleteEntry(Long id, User googleUser);
}
