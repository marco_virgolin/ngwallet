package tesina.virgolin.servlets;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Utils {
	
	private static Gson gson;
	
	public static Gson getGson(){
		if (gson == null){
			gson = new GsonBuilder().setDateFormat("MM/dd/yyyy").create();
		}
		return gson;
	}
	
	public static Long getIdFromPathInfo(String pathInfo) throws InvalidParametersException{
	    Pattern regExIdPattern = Pattern.compile("/([0-9]*)");
	    Matcher matcher = regExIdPattern.matcher(pathInfo);
	    if (matcher.find()) {
	    	return Long.parseLong(matcher.group(1));
	    }
	    throw new InvalidParametersException("Invalid URI, missing id");
	}
	
	
}
