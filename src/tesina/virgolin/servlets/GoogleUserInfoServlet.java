package tesina.virgolin.servlets;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.UserServiceFactory;

@SuppressWarnings("serial")
public class GoogleUserInfoServlet extends HttpServlet {
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)	throws IOException {
		String appUrl = "http://"+req.getServerName()+(req.getServerPort() == 80 ? "" : ":"+req.getServerPort());
		GoogleUserInfo userInfo = new GoogleUserInfo(UserServiceFactory.getUserService().getCurrentUser().getNickname(), 
				UserServiceFactory.getUserService().createLogoutURL(UserServiceFactory.getUserService().createLoginURL(appUrl)));
		resp.getWriter().print(Utils.getGson().toJson(userInfo));
		resp.getWriter().close();
	}
}

class GoogleUserInfo{
	String logoutUrl;
	String nickname;
	String loginUrl;
	GoogleUserInfo(String nickname, String logoutUrl){
		this.nickname = nickname;
		this.logoutUrl = logoutUrl;
	}
}

