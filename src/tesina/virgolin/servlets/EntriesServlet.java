package tesina.virgolin.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.UserServiceFactory;

import tesina.virgolin.data.DataService;
import tesina.virgolin.data.IDataService;
import tesina.virgolin.entities.EntryEntity;
import tesina.virgolin.entities.UserEntity;

@SuppressWarnings("serial")
public class EntriesServlet extends HttpServlet {

	private IDataService dataService = new DataService();
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("application/json");		
		List<EntryEntity> entries = dataService.getEntries(UserServiceFactory.getUserService().getCurrentUser());	
		resp.getWriter().print(Utils.getGson().toJson(entries));
		resp.getWriter().close();		
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("application/json");
		UserEntity user = new UserEntity(UserServiceFactory.getUserService().getCurrentUser().getEmail());
		EntryEntity entry;
		try{
			entry = buildEntityFromParameters(req, user, false);
			EntryEntity e = dataService.getEntry(dataService.saveEntry(entry));
			resp.getWriter().print(Utils.getGson().toJson(e));
		} catch (InvalidParametersException e){
			resp.getWriter().print(Utils.getGson().toJson(new Status(StatusType.ERROR, e.getMessage())));
		}
		resp.getWriter().close();		
	}
	
	public void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("application/json");
		UserEntity user = new UserEntity(UserServiceFactory.getUserService().getCurrentUser().getEmail());
		EntryEntity entry;		
		try{
			entry = buildEntityFromParameters(req, user, true);
			dataService.saveEntry(entry);
			resp.getWriter().print(Utils.getGson().toJson(new Status(StatusType.SUCCESS, "Entry updated")));
		} catch (InvalidParametersException e){
			resp.getWriter().print(Utils.getGson().toJson(new Status(StatusType.ERROR, e.getMessage())));
		}
		resp.getWriter().close();
	}
	
	public void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("application/json");		
		Long id;
		try {
			id = Utils.getIdFromPathInfo(req.getPathInfo());
			dataService.deleteEntry(id, UserServiceFactory.getUserService().getCurrentUser());
			resp.getWriter().print(Utils.getGson().toJson(new Status(StatusType.SUCCESS, "Entry deleted")));
		} catch (InvalidParametersException e){
			resp.getWriter().print(Utils.getGson().toJson(new Status(StatusType.ERROR, e.getMessage())));
		}
		resp.getWriter().close();
	}
	
	private EntryEntity buildEntityFromParameters(HttpServletRequest req, UserEntity user, boolean expectId) throws InvalidParametersException{
		Float amount;
		try { 
			amount = Float.parseFloat(req.getParameter("amount"));
		} catch (NumberFormatException | NullPointerException e){
			amount = null;
		}
		Date date;
		try {
			date = new SimpleDateFormat("M/d/yyyy").parse(req.getParameter("date"));
		} catch (NullPointerException | ParseException e) {
			date = null;
		}		
		String chapter = req.getParameter("chapter");
		String description = req.getParameter("description");
		if (date == null || amount == null || chapter.isEmpty() || chapter == null){
			throw new InvalidParametersException("Cannot create Entry, one or more parameters are missing");
		}
		EntryEntity entry = new EntryEntity(date, chapter, amount, description, user);
		if(expectId){
			Long id;			
			id = Utils.getIdFromPathInfo(req.getPathInfo());				
			entry.setId(id);			
		}		
		return entry;
	}
}

@SuppressWarnings("serial")
class InvalidParametersException extends Exception {
    public InvalidParametersException(String message) {
        super(message);
    }
}

enum StatusType {
	SUCCESS, ERROR
}

class Status {
	StatusType type;
	String message;
	Status(StatusType type, String message){
		this.type = type;
		this.message = message;
	}
}

