(function(){
	var app = angular.module('wallet-directives', ['ui.bootstrap','ngInputDate','wallet-data']);
	
	app.directive('editEntry', ['$locale','$modal','EntriesData',function($locale,$modal,EntriesData) {
	    return {
	    	restrict: 'A',
	        controller: function($scope){
	        	var originalEntry;
	    		$scope.editEntry = function (size){
	    			$scope.modalInstance = $modal.open({	    				
	    				templateUrl: 'editEntryModal.html',
	    				resolve: {
	    					entryScope: function(){
	    						return $scope;
	    					},
	    					entriesScope: function(){
	    						return $scope.$parent;
	    					}
	    				},
	    				controller: function($scope, $modalInstance, entryScope, entriesScope){
	    					originalEntry = {
	    		        			chapter: entryScope.entry.chapter,
	    		        			amount: entryScope.entry.amount,
	    		        			date: entryScope.entry.date,
	    		        			description: entryScope.entry.description
	    		        	};
	    					$scope.currencySymbol = $locale.NUMBER_FORMATS.CURRENCY_SYM;
	    					$scope.entry = entryScope.entry;
	    					EntriesData.getChapters($scope, entriesScope);
	    					$scope.save = function(){
	    						$modalInstance.close({entry:$scope.entry, entriesScope:entriesScope});				
	    					};
	    					$scope.cancel = function(){
	    						$modalInstance.dismiss();
	    					};
	    				},
	    				size: size
	    			});
	    			$scope.modalInstance.result.then(function(toSubmit){
	    				var entry = toSubmit.entry;
	    				var entriesScope = toSubmit.entriesScope;
	    				EntriesData.put(entry, entriesScope);
	    		    }, function(){
	    		    	$scope.entry.chapter = originalEntry.chapter;
	    		    	$scope.entry.amount = originalEntry.amount;
	    		    	$scope.entry.description = originalEntry.description;
	    		    	$scope.entry.date = originalEntry.date;
	    		    });
	    		}
	    	},
	    };
	}]);
	
	app.directive('deleteEntry', ['EntriesData', function(EntriesData) {
	    return {
	    	restrict: 'A',
	    	controller: function($scope){
	    		$scope.deleteEntry = function(){
	    			EntriesData.del($scope.entry.id, $scope);
	    		};
	    	}	    	
	    };
	}]);
	
	app.directive('createEntry', ['$locale','$modal','EntriesData',function($locale,$modal,EntriesData) {
		return {
	    	restrict: 'A',
	        controller: function($scope){
	    		$scope.createEntry = function (size) {
	    			$scope.modalInstance = $modal.open({	    				
	    				templateUrl: 'editEntryModal.html',
	    				resolve: {
	    					entriesScope: function(){
	    						return $scope;
	    					}
	    				},
	    				controller: function($scope, $modalInstance, entriesScope){
	    					$scope.currencySymbol = $locale.NUMBER_FORMATS.CURRENCY_SYM;
	    					EntriesData.getChapters($scope, entriesScope);
	    					$scope.entry = {
	    						chapter:'',
	    						description:'',
	    						date: new Date(),
	    					};
	    					$scope.save = function(){
	    						$modalInstance.close({entry:$scope.entry, entriesScope:entriesScope});				
	    					};
	    					$scope.cancel = function(){
	    						$modalInstance.dismiss();
	    					};
	    				},
	    				size: size
	    			});
	    			$scope.modalInstance.result.then(function(toSubmit){
	    				var entry = toSubmit.entry;
	    				var entriesScope = toSubmit.entriesScope;
	    				EntriesData.post(entry, entriesScope);
	    		    }, function(){
	    		    	return;
	    		    });
	    		}
	    	},
	    };
	}]);	
	
	app.directive('filters', ['EntriesData', function(EntriesData){
		return {
			restrict:'A',
			controller: function($scope){
				$scope.isCollapsed = true;
				$scope.isFiltersActive = false;
				$scope.filter = {
						chapter: undefined,
						description: undefined,
						datefrom: undefined,
						dateTo: undefined,
						amountType: undefined
				};
				$scope.exceptEmptyComparator = function(actual, expected){
					if (!expected) {
						return true;
				    }
				    return angular.equals(expected, actual);
				};
				$scope.deleteFilters = function(){
					$scope.filter = undefined;
				};
				$scope.$watchCollection('entries',function(){
					EntriesData.getChapters($scope, $scope);
				});
				$scope.$watchCollection('filter',function(){
					if($scope.filter && ($scope.filter.chapter || $scope.filter.description ||
							$scope.filter.dateFrom || $scope.filter.dateTo || $scope.filter.amountType)){
						$scope.isFiltersActive = true;
					} else {
						$scope.isFiltersActive = false;
					}
				});
			}
		};
	}]);
	
	app.directive('currencyInput',function(){
	    return {
	    	restrict:'A',
	    	link:function(scope,ele,attrs){
	            ele.bind('keypress',function(e){
	            	if(ele.get(0).selectionStart != ele.get(0).selectionEnd)
	            		return;
	            	var cursorPosition = ele.get(0).selectionStart;
	            	var newVal = $(this).val().substr(0, cursorPosition) + (e.charCode!==0?String.fromCharCode(e.charCode):'') + $(this).val().substr(cursorPosition);
	            	if($(this).val().search(/(.*)(\.|,)[0-9][0-9]/)===0 && newVal.length>$(this).val().length){
                       	var oldFloatVal = parseFloat($(this).val().replace(',','.'));
                    	var newFloatVal = parseFloat(newVal.replace(',','.'));
	                    if(!isNaN(oldFloatVal) && !isNaN(newFloatVal)){
	            			if(countDecimalPlaces(newFloatVal)>countDecimalPlaces(oldFloatVal)
	            					|| (countDecimalPlaces(newFloatVal)===countDecimalPlaces(oldFloatVal) && String.fromCharCode(e.charCode)==='0'))
	            				e.preventDefault();
	           			}
                    }
	            });
	        }
	    };
	});
	
	function countDecimalPlaces(number) { 
		var str = "" + number;
		var index = str.indexOf('.');
		if (index >= 0) {
			return str.length - index - 1;
		} else {
			return 0;
		}
	}
})();