(function(){	
	var app = angular.module('wallet', ['wallet-directives','wallet-data','wallet-filters']);
	
	app.controller('UserInfoCtrl', ['UserData',function(UserData){
		UserData.get(this);
	}]);
	
	app.controller('WalletCtrl', ['$scope','$locale','EntriesData', function($scope, $locale, EntriesData){
		$scope.entries = [];
		$scope.filteredEntries = [];
		$scope.balance = 0;
		$scope.filteredBalance = 0;
		EntriesData.get($scope);
		$scope.$watchCollection('entries',function(){
			EntriesData.getEntriesBalance($scope, false);
		});
		$scope.$watchCollection('filteredEntries',function(){
			EntriesData.getEntriesBalance($scope, true);
		});
		$scope.sortType = '-date';
		$scope.setSortType = function(type){
			switch(type){
				case 'date': 
					if($scope.sortType == 'date')
						$scope.sortType = '-date';
					else if ($scope.sortType == '-date')
						$scope.sortType = 'date';
					else $scope.sortType = '-date';
					break;
				case 'chapter':
					if($scope.sortType == 'chapter')
						$scope.sortType = '-chapter';
					else if ($scope.sortType = '-chapter')
						$scope.sortType = 'chapter';
					else $scope.sortType = 'chapter';
					break;
				case 'amount': 
					if($scope.sortType == '-amount')
						$scope.sortType = 'amount';
					else if ($scope.sortType == 'amount')
						$scope.sortType = '-amount';
					else $scope.sortType = '-amount';
					break;
				default:
					$scope.sortType = '-date';
					break;
			}
		}
	}]);
})();



