(function(){
	angular.module('wallet-filters', []).filter('amountFilter',function(){
		return function(entries, amountType){
			if (!amountType)
				return entries;
			var filteredEntries = [];
			angular.forEach(entries, function(entry){
				if (amountType=='expenditure' && parseFloat(entry.amount) < 0) {
					filteredEntries.push(entry);
			    } else if (amountType=='income' && parseFloat(entry.amount) >= 0){
			    	filteredEntries.push(entry);
			    }
			});
			return filteredEntries;
		};
	}).filter('dateFilter',function(){
		return function(entries, comparator){
			var dateFrom = comparator.from;
			var dateTo = comparator.to;
			var filteredEntries = [];
			if(!dateFrom && !dateTo)
				return entries;
			else if(!dateTo){
				angular.forEach(entries, function(entry){
					if(entry.date >= dateFrom)
						filteredEntries.push(entry);
				});
			} else if(!dateFrom){
				angular.forEach(entries, function(entry){
					if(entry.date <= dateTo)
						filteredEntries.push(entry);
				});
			} else {
				angular.forEach(entries, function(entry){
					if (entry.date >= dateFrom && entry.date <= dateTo)
						filteredEntries.push(entry);
				});
			}
			return filteredEntries;			
		};
	});
})();