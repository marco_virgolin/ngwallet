(function(){
	angular.module('wallet-data', ['wallet-messages']).factory('EntriesData', ['$http','WalletMessages',function($http,WalletMessages){
		return {
			get: function(scope){
				WalletMessages.showLoading();
				$http.get('/entries').success(function(result){
					WalletMessages.hideLoading();
					if(result.type && result.type == 'ERROR'){
						WalletMessages.alertError(result.message);	
						return;
					}
					/*set entry.date as Date objects*/
					angular.forEach(result, function(entry){
						entry.date = new Date(entry.date);
					});
					scope.entries = result;
				}).error(function(data, status, headers, config) {
					WalletMessages.hideLoading();
					WalletMessages.alertError('An error occurred, code '+status)
			    });
			},
			post: function(entry, scope){
				WalletMessages.showLoading();
				if(typeof(entry.amount)=='string')
					entry.amount = parseFloat(entry.amount.replace(',','.'));
				entry.date = entry.date.getMonth()+1+"/"+entry.date.getDate()+"/"+entry.date.getFullYear();
				$http({
					method:'POST',
					url: '/entries',
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: $.param(entry)
				}).success(function(result){
					WalletMessages.hideLoading();
					if(result.type && result.type == 'ERROR'){
						WalletMessages.alertError(result.message);	
						return;
					}
					result.date = new Date(result.date);
					scope.entries.push(result);
				}).error(function(data, status, headers, config) {
					WalletMessages.hideLoading();
					WalletMessages.alertError('An error occurred, code '+status);
			    });
			},
			put: function(entry, scope){
				WalletMessages.showLoading();
				if(typeof(entry.amount)=='string')
					entry.amount = parseFloat(entry.amount.replace(',','.'));
				var entryOriginalDate = entry.date;
				var entryToSubmit = entry;
				entryToSubmit.date = entry.date.getMonth()+1+"/"+entry.date.getDate()+"/"+entry.date.getFullYear();
				$http({
					method:'PUT',
					url: '/entries/'+entry.id,
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: $.param(entryToSubmit)
				}).success(function(result){
					WalletMessages.hideLoading();
					if(!(result.type && result.type == 'ERROR'))
						return;
					WalletMessages.alertError(result.message);
				}).error(function(data, status, headers, config) {
					WalletMessages.hideLoading();
					WalletMessages.alertError('An error occurred, code '+status)
			    });
				entry.date = entryOriginalDate;
				// update balance, since $watchCollection doesn't trigger when you update fields of an element
				this.getEntriesBalance(scope,false);
				this.getEntriesBalance(scope,true);
			},
			del: function(entryId, scope){
				WalletMessages.showLoading();
				$http({
    				method:'DELETE',
    				url: '/entries/'+entryId,
    				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    			}).success(function(result){
    				WalletMessages.hideLoading();
    				if(result.type && result.type == 'ERROR'){
						WalletMessages.alertError(result.message);	
						return;
					}
    				for(var i in scope.entries){
    					if(scope.entries[i].id == entryId){
    						scope.entries.splice(i, 1);
    						return;
    					}
    				}
    			}).error(function(data, status, headers, config) {
    				WalletMessages.hideLoading();
					WalletMessages.alertError('An error occurred, code '+status)
			    });
			},
			getChapters: function(scope, entriesScope){
				scope.chapters = [];
				angular.forEach(entriesScope.entries, function(entry){
					if(scope.chapters.indexOf(entry.chapter) === -1)
							scope.chapters.push(entry.chapter);
				});
			},
			getEntriesBalance: function(scope, wantFilteredBalance){
				if(!wantFilteredBalance){
					scope.balance = 0;
					angular.forEach(scope.entries, function(entry){
						scope.balance += entry.amount;
					});
				} else {
					scope.filteredBalance = 0;
					angular.forEach(scope.filteredEntries, function(filteredEntry){
						scope.filteredBalance += filteredEntry.amount;
					});
				}
			}
		};
	}]).factory('UserData', ['$http','WalletMessages',function($http,WalletMessages){
		return {
			get: function(scope){
				$http.get('/userinfo').success(function(result){
					scope.userInfo = result;
				}).error(function(data, status, headers, config) {
					WalletMessages.alertError('An error occurred, code '+status)
			    });
			}
		};
	}]);
})();
