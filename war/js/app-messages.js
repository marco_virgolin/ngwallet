(function(){
	angular.module('wallet-messages', []).factory('WalletMessages', ['$timeout',function($timeout){
		var $alertsContainer = $('#alerts');
		var $errorMessageSpan = $('#errorMessage');
		var $loadingContainer = $('#loading');
		var timeoutPromise;
		var ALERT_DURATION = 5000;
		return {
			alertError: function(message){
				$errorMessageSpan.text(message);
				if(timeoutPromise)
					$timeout.cancel(timeoutPromise);
				$alertsContainer.slideDown(function(){
					timeoutPromise = $timeout(function(){
						$alertsContainer.slideUp();
					}, ALERT_DURATION);
				})
			},
			showLoading: function(){
				$loadingContainer.show();
			},
			hideLoading: function(){
				$loadingContainer.hide();
			}
		};
	}]);
})();